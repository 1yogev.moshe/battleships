public class Ship {

    private int len;
    private Integer [][] xyCoordinates;
    private boolean isDrown;

    public Ship(int len) {
        this.len = len;
        this.xyCoordinates = new Integer[len][2];
        this.isDrown = false;
    }


    public int getLen() {
        return this.len;
    }

    public Integer[][] getXyCoordinates() {
        return this.xyCoordinates;
    }

    public boolean getIsDrown() {
        return this.isDrown;
    }

    public void setDrawn() {
        if (!this.isDrown) this.isDrown = true;
        else System.out.println("The chip has already drown");
    }


    public void initializeCoordinates(Integer [] startsAt, Integer [] direction) { // DIRECTION -> (1,0): right, (-1,0): left, (0,1): up, (0,-1): down

        int y = startsAt[1];
        int x = startsAt[0];

        int directionY = startsAt[1];
        int directionX = startsAt[0];

        int endX = -1;
        int endY = -1;

        if (directionX == -1) endX = x - this.len;
        if (directionX == 1) endX = x + this.len;

        if (directionY == -1) endY = y - this.len;
        if (directionY == 1) endY = y + this.len;

        int cellIndex = 0;
        boolean checking = true;

        while (checking) {

            try {
                this.xyCoordinates[cellIndex][0] = x;
                this.xyCoordinates[cellIndex][1] = y;
                cellIndex++;
            } catch (Exception e) {
                checking = false;
            }

            x += direction[0];
            y += direction[1];

        }


    }

    public static void main(String[] args) {
        Ship b = new Ship(5);
        Integer [] startsAt = {0,0};
        Integer [] direction = {1,0};
        b.initializeCoordinates(startsAt, direction);
        MyArray.print(b.getXyCoordinates());
    }
    
}
