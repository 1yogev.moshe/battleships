import java.util.Random;
import java.util.Scanner;

public class Game {

    private static Scanner sc = new Scanner(System.in); // LOCAL SCANNER
    private static Random rand = new Random(); // LOCAL RANDOM GENERATOR

    private Board player1Board1 = new Board(10); // USED TO HOLD PLAYER1'S BOATS
    private Board player1Board2 = new Board(10); // USED TO SEE PLAYER1'S GUESSES

    private Board player2Board1 = new Board(10); // USED TO HOLD PLAYER2'S BOATS
    private Board player2Board2 = new Board(10); // USED TO SEE PLAYER2'S GUESSES

    // private Character [][] player1Board1c = {{'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', 'V', 'V', 'V', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', 'V', 'V', 'V', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', 'V', 'V', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'}};

    // private Character [][] player2Board1c = {{'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', 'V', 'V', 'V', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', 'V', 'V', 'V', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', 'V', 'V', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'}};


    private Player player1; // PLAYER 1
    private BotiBot botiBot; // PLAYER 2

       

    public Game() { // CONSTRUCTOR

        // this.player1Board1.setBoard(player1Board1c);
        // this.player2Board1.setBoard(player2Board1c);

        System.out.println("player 1 please enter your name : ");
        String name1 = sc.next();

        System.out.println("player 2 please enter your name : ");
        String name2 = sc.next();

       this.player1 = new Player(name1);
    //    this.botiBot = new BotiBot(this.player1Board1);

       int turn = 1 + rand.nextInt(1);

       if (turn == 1) this.player1.setTurn();
       if (turn == 2) this.botiBot.setTurn();

    }

    public void run() { // THE RUNNING METHOD - GAME PROGRESS

        player1Board1.placeBoats(player1);
        player2Board1.placeBoats(botiBot);
        boolean running = true;

        while (running) {

            if (running) {
                System.out.println(player1.getName() + "'s turn:");
                System.out.println("your board:");
                player1Board1.printBoard();
                System.out.println("enemy board:");
                player2Board2.printBoard();
                this.AttackingTurn(player1, player2Board1, player2Board2);
                if (this.checkForWin(this.player1.getPlayerDrowns())) { // if (player2Board1.checkForWin(this.player1.getPlayerDrowns())) {
                    System.out.println(player1.getName() + " won!");
                    running = false;
                }

            }

            // sc.nextLine();

            if (running) {
                System.out.println(botiBot.getName() + "'s turn:");
                System.out.println("your board:");
                player2Board1.printBoard();
                System.out.println("enemy board:");
                player1Board2.printBoard();
                this.AttackingTurn(botiBot, player1Board1, player1Board2);
                if (this.checkForWin(this.botiBot.getPlayerDrowns())) { // if (player1Board1.checkForWin(this.player2.getPlayerDrowns())) {
                    System.out.println(botiBot.getName() + " won!");
                    running = false;
                }


            }

            // sc.nextLine();
        }
        

    }

    private void AttackingTurn(Player player, Board board, Board enemyBoard) { // PROTOCOL FOR ATTACKING DEPENDING ON THE PLAYER
        //enemyBoard.printBoard();
        boolean gotInput = false;

        Integer[] xy = {-1,-1};

        while (!gotInput) {
            xy = player.getInput(board.getBoard(), player) ;
            if (board.getCellValue(xy) == 'O') {
                System.out.println("coordinates were already chosen, choose other coordinates");

            } else if (board.getCellValue(xy) == 'X') {
                System.out.println("coordinates were already chosen, choose other coordinates");

            } else {
                gotInput = true;
            }
            
        }
        if (board.checkForHit(xy)){
            System.out.println("Hit!");
            enemyBoard.changeCellValue(xy, 'X');
            board.changeCellValue(xy, 'X');
            // board.checkForHitHandler(xy);
            player.setPlayerDrowns(player.getPlayerDrowns() + 1);
        }
        else {
            System.out.println("Miss!");
            enemyBoard.changeCellValue(xy, 'O');
            board.changeCellValue(xy, 'O');
        }

        enemyBoard.printBoard();
    }


    private boolean checkForWin(int drowns) { // CHECKING FOR END GAME

        return (drowns == 17);
    }
    

    public static void main(String[] args) {
        Game g = new Game();
        g.run();
    }


}
