# `battleShips`

## Game Description:
In battleships there's a 10x10 board, which each player has 5 boats - first is 5 length, 4, 3, 3, and 2. The players choose where to place them on the board, the other player's mission is to detect the boats and hit them.

## Requierments
- board.
- boats placing phase.
- finding other player's boats.
- check win.

## Input And Output Examples

### Placing Boats Phase

`Output:`

    Place the 5 length ship (starting point) : 
    Enter x value (1-10) : 

`Input:` 2

`Input:` 11 `Output:` Index out of bounds.

`Input:` 2 (already taken) `Output:` Index is already taken.

    Enter y value (1-10) : 


`Input:` 5

    What direction is the ship facing ? (right, left ... [this wil depend on where is possible]): 

`Input:` right

`Input:` left `Output:` There's no space avaiable.

    Your Board:

       1 2 3 4 5 6 7 8 9 10 
    1  - - - - - - - - - -
    2  - - - - - - - - - -
    3  - - - - - - - - - -
    4  - - - - - - - - - -
    5  - X X X X X - - - -
    6  - - - - - - - - - -
    7  - - - - - - - - - -
    8  - - - - - - - - - -
    9  - - - - - - - - - -
    10 - - - - - - - - - -


### Attacking

`Output:`

    Opponent's Board:              Your Board:
                                       
       1 2 3 4 5 6 7 8 9 10           1 2 3 4 5 6 7 8 9 10 
    1  - - - - - - - - - -         1  - - - - - - - - - -        
    2  - - - - - - - - - -         2  - - - - - - - - - -       
    3  - - - - - - - - - -         3  - - - - - - - - - -     
    4  - - - - - - - - - -         4  - - - - - - - - - -              
    5  - - - - - - - - - -         5  - X X X X X - - - -  
    6  - - - - - - - - - -         6  - - - - - - - - - -      
    7  - - - - - - - - - -         7  - - - - - - - - - -  
    8  - - - - - - - - - -         8  - - - - - - - - - - 
    9  - - - - - - - - - -         9  - - - - - - - - - -  
    10 - - - - - - - - - -         10 - - - - - - - - - -  

    X - hit
    O - miss

    Enter x coordinate to check (1 - 10): 

`Input:` 5

    Enter y coordinate to check (1 - 10): 

`Input:` 6

`Output:` 

    miss
    
    Opponent's Board:              Your Board:
                                       
       1 2 3 4 5 6 7 8 9 10           1 2 3 4 5 6 7 8 9 10 
    1  - - - - - - - - - -         1  - - - - - - - - - -        
    2  - - - - - - - - - -         2  - - - - - - - - - -       
    3  - - - - - - - - - -         3  - - - - - - - - - -     
    4  - - - - - - - - - -         4  - - - - - - - - - -              
    5  - - - - - - - - - -         5  - X X X X X - - - -  
    6  - - - - O - - - - -         6  - - - - - - - - - -      
    7  - - - - - - - - - -         7  - - - - - - - - - -  
    8  - - - - - - - - - -         8  - - - - - - - - - - 
    9  - - - - - - - - - -         9  - - - - - - - - - -  
    10 - - - - - - - - - -         10 - - - - - - - - - -  

    X - hit
    O - miss

`Output:` 

    hit

    Opponent's Board:              Your Board:
                                       
       1 2 3 4 5 6 7 8 9 10           1 2 3 4 5 6 7 8 9 10 
    1  - - - - - - - - - -         1  - - - - - - - - - -        
    2  - - - - - - - - - -         2  - - - - - - - - - -       
    3  - - - - - - - - - -         3  - - - - - - - - - -     
    4  - - - - - - - - - -         4  - - - - - - - - - -              
    5  - - - - - - - - - -         5  - X X X X X - - - -  
    6  - - - - X - - - - -         6  - - - - - - - - - -      
    7  - - - - - - - - - -         7  - - - - - - - - - -  
    8  - - - - - - - - - -         8  - - - - - - - - - - 
    9  - - - - - - - - - -         9  - - - - - - - - - -  
    10 - - - - - - - - - -         10 - - - - - - - - - -  

    X - hit
    O - miss


### Game Over 
`Output:` 

    Opponent's Board:              Your Board:
                                       
       1 2 3 4 5 6 7 8 9 10           1 2 3 4 5 6 7 8 9 10 
    1  - - - - X - - O - -         1  - - - - - - - - - -        
    2  - O - - x - - - - -         2  - - O - - - O - - -       
    3  - X - - X - - O - X         3  - - - - - - - - - -     
    4  - X - - X - - - - X         4  - - - - - - - - - -              
    5  - X - - X - - O - -         5  - X X X X X - - - -  
    6  - - - - X - - - - -         6  - - - - - - - - - -      
    7  - - O - - - - O - -         7  - - - - - O - - - -  
    8  - X X X - - - - - -         8  - - - - - - - - - - 
    9  - - O - - - X X X X         9  - O - - - - - - O -  
    10 O - - - - O - - - -         10 - - - - - - - - - -  

    X - hit
    O - miss

    Game Over 
    You Won!



### Class:  `Boat`
#### Attributes
 - `private int len;` // ship's length
 - `private Integer [][] xyCoordinates;` // all ship's coordinates
 - `private Character [] xyCoordinatesValues;` // ALL THE VALUES ON THE BOARD IN THESE COORDINATES 
 - `private boolean drown;` // represents if the ship has been drown
 - `private int index;` // THE INDEX FOR LOPPING THROUGH xyCoordinatesValues

#### Methods

| `Role`                                              | `In Code`                                                 |
| ----------------------------------------------------|:---------------------------------------------------------:|
| initializing the ship's coordinates   | public void initializeCoordinates(Integer [] startsAt, Integer [] direction)                                  |               
| checking if the ship is drown | public boolean isDrown() |     
| SETTING ALL THE VALUES ON THE BOARD IN THESE COORDINATES  | public void setXyCoordinatesValues(Character sign, int Index)  |     
| GETTING ALL THE VALUES ON THE BOARD IN THESE COORDINATES  | public Character[] getXyCoordinatesValues() |  
  




### Class:  `Board`
#### Attributes
 - `private Character [][] board;` // A BOARD IN A GIVEN DIMENSIONS
 - `private Boat [] boats;` // ALL THE BOATS WHICH ARE ON THIS BOARD
 - `private static Scanner sc = new Scanner(System.in);` // LOCAL SCANNER

#### Methods

| `Role`                                              | `In Code`                                                 |
| ----------------------------------------------------|:---------------------------------------------------------:|
| printing the board in a good format | public void printBoard() |
| setting all array's cells to be '-' | private void initializeBoard(Character [][] board) |
| check for hit or miss | public boolean checkForHit(Integer [] xy) |
| changing cell value | public void changeValueAt(Integer [] xy, char sign) |
| checking for win | public boolean checkForWin(int drowns) | 
| get cell value | public char getCellValue(Integer [] xy) |
| HANDLING WHEN HIT OCCURS | public void checkForHitHandler(Integer[] xy) |
| PLACING ALL THE BOATS ON THE BOARD | public void placeBoats(Player player) |
| PLACING ONE BOAT | public void getXYInput(Player player, Integer [] boatsLength, int boatsLengthIndex, int boatIndex) |
| getting an integer input | public int getInteger(char sign, Player player) |
| checks the limits of the board | public int checkDirection(Integer [][] tempCoordinates) |
| getting a direction from an input | public Integer [] getDirection(String direction) |
| checks the direction input | public boolean isGood(String direction) |
| UPDATING THE BOARD - GIVEN COORDINATES => A SPECIFIC VALUE | public void updateBoard(Boat b) |



### Class:  `Game`
#### Attributes

 - `private static Scanner sc = new Scanner(System.in);` // LOCAL SCANNER
 - `private static Random rand = new Random();` // LOCAL RANDOM GENERATOR

 - `private Board player1Board1 = new Board(10);` // USED TO HOLD PLAYER1'S BOATS
 - `private Board player1Board2 = new Board(10);` // USED TO SEE PLAYER1'S GUESSES

 - `private Board player2Board1 = new Board(10);` // USED TO HOLD PLAYER2'S BOATS
 - `private Board player2Board2 = new Board(10);` // USED TO SEE PLAYER2'S GUESSES

 - `private Player player1;` // PLAYER 1
 - `private Player player2;` // PLAYER 2


#### Methods

| `Role`                                              | `In Code`                                                 |
| ----------------------------------------------------|:---------------------------------------------------------:|
| THE RUNNING METHOD - GAME PROGRESS | pubic void run() |
| PROTOCOL FOR ATTACKING DEPENDING ON THE PLAYER | private void AttackingTurn(Player player, Board board, Board enemyBoard) |
| CHECKS FOR END GAME | private boolean checkForWin(int drowns) |



### Class:  `Player`
#### Attributes
 - `private String name;` // PLAYER'S NAME
 - `private int score;` // PLAYER'S SCORE
 - `private static Scanner sc = new Scanner(System.in);` // LOCAL SCANNER
 - `private boolean turn;` // PLAYERS TURN
 - `private int playerDrowns;` // how many hits



#### Methods

| `Role`                                              | `In Code`                                                 |
| ----------------------------------------------------|:---------------------------------------------------------:|
| getting an integer input | public int getInteger(char sign) |
|  enemy's full board(not hidden one)| public Integer [] getInput(Character[][] board) |
