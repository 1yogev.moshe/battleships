import java.util.Scanner;


public class Player {

    private String name; // PLAYER'S NAME
    private int score; // PLAYER'S SCORE
    private static Scanner sc = new Scanner(System.in); // LOCAL SCANNER
    private boolean turn; // PLAYERS TURN
    private int playerDrowns; // how many hits

    /**
     * 
     * @param name the player's name
     */
    public Player(String name) { // CONSTRUCTOR
        this.name = name;
        this.score = 0;
        this.turn = false;
        this.playerDrowns = 0;
    }

    /**
     * 
     * @return the player's current drowns
     */
    public int getPlayerDrowns() {
        return playerDrowns;
    }

    /**
     * 
     * @param playerDrowns the new player's drowns 
     */
    public void setPlayerDrowns(int playerDrowns) {
        this.playerDrowns = playerDrowns;
    }

    /**
     * 
     * @return whether its player's turn    
     */
    public boolean getTurn() { // RETURNING WHETHER IT'S PLAYER TURN
        return this.turn;
    }

    /**
     * 
     */
    public void setTurn() { // UPDATING THE TURN STATUS AFTER A MOVE
        if (this.turn) this.turn = false;
        else this.turn = true;
    }

    /**
     * 
     * @return player's name
     */
    public String getName() { // GETTING PLAYER'S NAME
        return name;
    }

    /**
     * 
     * @param name the new name
     */
    public void setName(String name) { // SETTING PLAYER'S NAME
        this.name = name;
    }

    /**
     * 
     * @return returns player's score
     */
    public int getScore() { // GETTING PLAYER'S SCORE
        return score;
    }

    /**
     * 
     * @param score the new player's score
     */
    public void setScore(int score) { // SETTING PLAYER'S SCORE
        this.score = score;
    }
    
    /**
     * 
     */
    @ Override
    public String toString() { // TOSTRING - ALL PLAYER'S INFO
        return "Player {name= " + this.getName() + ", score= " + this.getScore() + "}";
    }


    /**
     * 
     * @param board the board we are dealing with
     * @return the xy coordinate the player chose
     */
    public Integer [] getInput(Character[][] board) { //enemy's full board(not hidden one)
        

        Integer [] xy = new Integer[2];

        boolean works = false;
        boolean worksX = false;
        boolean worksY = false;
        int x = -1;
        int y = -1;


        while (!works) {


            while (!worksX) {

                x = this.getInteger('x');
                
                if (x < 1 || x > board[0].length) System.out.println(this.getName() + " Index out of range.");
                else worksX = true;

            }
            
            
            while (!worksY) {

                y = this.getInteger('y');
                
                if (y < 1 || y > board[0].length) System.out.println(this.getName() + " Index out of range.");
                else worksY = true;

            }

            xy[0] = x -1;
            xy[1] = y -1;

            if (board[xy[1]][xy[0]] == 'O' || board[xy[1]][xy[0]] == 'X') {
                System.out.println(this.getName() + " Cell is already taken.");
                worksX = false;
                worksY = false;
            }
            else works = true;

        }
        

        return xy;

    }


    /**
     * getting an integer input
     * @param sign the sign of the value your getting
     * @return the value we got
     */
    public int getInteger(char sign) { // getting an integer input
        boolean integ = false;
        int value = 0;

        while (!integ) {
                    
            try {
                System.out.println(this.getName() + " Enter " + sign + " value (1-10) : ");
                String str = sc.next();
                value = Integer.parseInt(str);
                integ = true;
            } catch (Exception e) {
                System.out.println(this.getName() + " Please enter an integer.");

            }
            
        }

        return value;
    }

    // public static void main(String[] args) {
    //     Player p1 = new Player("Ray");
    //     Board b1 = new Board(10);
    //     Integer[] xy = p1.getInput(b1.getBoard(), p1);
    //     MyArray.print(xy);
    // }


}
