import java.util.Scanner;


public class Player {

    private String name; // PLAYER'S NAME
    private int score; // PLAYER'S SCORE
    private static Scanner sc = new Scanner(System.in); // LOCAL SCANNER
    private boolean turn; // PLAYERS TURN
    private int playerDrowns; // how many hits

    public Player(String name) { // CONSTRUCTOR
        this.name = name;
        this.score = 0;
        this.turn = false;
        this.playerDrowns = 0;
    }

    public int getPlayerDrowns() {
        return playerDrowns;
    }

    public void setPlayerDrowns(int playerDrowns) {
        this.playerDrowns = playerDrowns;
    }

    public boolean getTurn() { // RETURNING WHETHER IT'S PLAYER TURN
        return this.turn;
    }

    public void setTurn() { // UPDATING THE TURN STATUS AFTER A MOVE
        if (this.turn) this.turn = false;
        else this.turn = true;
    }

    public String getName() { // GETTING PLAYER'S NAME
        return name;
    }

    public void setName(String name) { // SETTING PLAYER'S NAME
        this.name = name;
    }

    public int getScore() { // GETTING PLAYER'S SCORE
        return score;
    }

    public void setScore(int score) { // SETTING PLAYER'S SCORE
        this.score = score;
    }
    
    @ Override
    public String toString() { // TOSTRING - ALL PLAYER'S INFO
        return "Player {name= " + this.getName() + ", score= " + this.getScore() + "}";
    }

    public Integer [] getInput(Character[][] board, Player player) { //enemy's full board(not hidden one)
        

        Integer [] xy = new Integer[2];

        boolean works = false;
        boolean worksX = false;
        boolean worksY = false;
        boolean integ = false;

        int x = -1;
        int y = -1;


        while (!works) {


            while (!worksX) {

                integ = false;

                while (!integ) {
                    
                    try {
                        System.out.println(player.getName() + " Enter x value (1-10) : ");
                        String str = sc.next();
                        x = Integer.parseInt(str);
                        integ = true;
                    } catch (Exception e) {
                        System.out.println(player.getName() + " Please enter an integer.");

                    }
                    
                }
                
                if (x < 1 || x > board[0].length) System.out.println(player.getName() + " Index out of range.");
                else worksX = true;

            }
            
            
            while (!worksY) {

                integ = false;

                while (!integ) {
                    
                    try {
                        System.out.println(player.getName() + " Enter y value (1-10) : ");
                        String str = sc.next();
                        y = Integer.parseInt(str);
                        integ = true;
                    } catch (Exception e) {
                        System.out.println(player.getName() + " Please enter an integer.");

                    }
                    
                }
                
                if (y < 1 || y > board[0].length) System.out.println(player.getName() + " Index out of range.");
                else worksY = true;

            }

            xy[0] = x -1;
            xy[1] = y -1;

            if (board[xy[1]][xy[0]] == 'O' || board[xy[1]][xy[0]] == 'X') {
                System.out.println(player.getName() + " Cell is already taken.");
                worksX = false;
                worksY = false;
            }
            else works = true;

        }
        

        return xy;

    }

    // public static void main(String[] args) {
    //     Player p1 = new Player("Ray");
    //     Board b1 = new Board(10);
    //     Integer[] xy = p1.getInput(b1.getBoard(), p1);
    //     MyArray.print(xy);
    // }


}
