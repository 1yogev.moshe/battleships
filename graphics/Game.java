import java.awt.event.MouseEvent;
import java.util.Random;
import java.util.Scanner;
import javax.swing.*;
import javax.swing.event.MouseInputListener;

import java.awt.*;
import java.awt.event.MouseEvent;


import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

public class Game extends JPanel implements MouseInputListener {

    private static Scanner sc = new Scanner(System.in); // LOCAL SCANNER
    private static Random rand = new Random(); // LOCAL RANDOM GENERATOR

    private Board player1Board1 = new Board(10); // USED TO HOLD PLAYER1'S BOATS
    private Board player1Board2 = new Board(10); // USED TO SEE PLAYER1'S GUESSES

    private Board player2Board1 = new Board(10); // USED TO HOLD PLAYER2'S BOATS
    private Board player2Board2 = new Board(10); // USED TO SEE PLAYER2'S GUESSES

    // private Character [][] player1Board1c = {{'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', 'V', 'V', 'V', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', 'V', 'V', 'V', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', 'V', 'V', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'}};

    // private Character [][] player2Board1c = {{'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', 'V', 'V', 'V', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'V', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', 'V', 'V', 'V', '-', '-', '-', '-', '-'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', 'V', 'V', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'},
    //                                         {'-', '-', '-', '-', '-', '-', '-', '-', '-', 'V'}};


    private Player player1; // PLAYER 1
    private Player player2; // PLAYER 2

    // -----------------------------------------------------------------------------------------
    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;
    private final Integer [][] cellValues = new Integer[10][4];
    private Integer [] onBoard = new Integer[2];

    private Integer [][] hits = new Integer[17][2];
    private int hitsIndex = 0;
    
    private Integer [][] misses = new Integer[100-17][2];
    private int missesIndex = 0;

    private Boat [] boats = new Boat[5];


    Image boardImage;
    Image hitImage;
    Image missImage;
    Image boatImage;

    private int tileLength;

    // -----------------------------------------------------------------------------------------

       

    public Game() { // CONSTRUCTOR

        // this.player1Board1.setBoard(player1Board1c);
        // this.player2Board1.setBoard(player2Board1c);

        // System.out.println("player 1 please enter your name : ");
        // String name1 = sc.next();

        // System.out.println("player 2 please enter your name : ");
        // String name2 = sc.next();

       this.player1 = new Player("name1");
       this.player2 = new Player("name2");

       int turn = 1 + rand.nextInt(1);

       if (turn == 1) this.player1.setTurn();
       if (turn == 2) this.player2.setTurn();


        this.SCREEN_HEIGHT = 500;
        this.SCREEN_WIDTH = 1200;

        this.tileLength = 50;


        this.boardImage = new ImageIcon("res/board.png").getImage();
        this.hitImage = new ImageIcon("res/hit.png").getImage();
        this.missImage = new ImageIcon("res/miss.png").getImage();
        this.boatImage = new ImageIcon("res/boat.png").getImage();
        

        this.setPreferredSize(new Dimension(this.SCREEN_WIDTH,this.SCREEN_HEIGHT));

        int index = 0;
        for (int i = 0; i < this.tileLength*10; i+=this.tileLength) {
            this.cellValues[index][0] = i; // x1
            this.cellValues[index][1] = i; // y1
            this.cellValues[index][2] = i+this.tileLength; // x2
            this.cellValues[index][3] = i+this.tileLength; // y2
            index++;
        }
        

        this.addMouseListener(this);
        

        // this.run();

    }

    public void run() { // THE RUNNING METHOD - GAME PROGRESS

        player1Board1.placeBoats(player1);
        player2Board1.placeBoats(player2);
        boolean running = true;

        while (running) {

            if (running) {
                System.out.println(player1.getName() + "'s turn:");
                System.out.println("your board:");
                player1Board1.printBoard();
                System.out.println("enemy board:");
                player2Board2.printBoard();
                this.AttackingTurn(player1, player2Board1, player2Board2);
                if (this.checkForWin(this.player1.getPlayerDrowns())) { // if (player2Board1.checkForWin(this.player1.getPlayerDrowns())) {
                    System.out.println(player1.getName() + " won!");
                    running = false;
                }

            }

            // sc.nextLine();

            if (running) {
                System.out.println(player2.getName() + "'s turn:");
                System.out.println("your board:");
                player2Board1.printBoard();
                System.out.println("enemy board:");
                player1Board2.printBoard();
                this.AttackingTurn(player2, player1Board1, player1Board2);
                if (this.checkForWin(this.player2.getPlayerDrowns())) { // if (player1Board1.checkForWin(this.player2.getPlayerDrowns())) {
                    System.out.println(player2.getName() + " won!");
                    running = false;
                }


            }

            // sc.nextLine();
            repaint();
        }
        

    }

    private void AttackingTurn(Player player, Board board, Board enemyBoard) { // PROTOCOL FOR ATTACKING DEPENDING ON THE PLAYER
        enemyBoard.printBoard();
        boolean gotInput = false;

        Integer[] xy = {-1,-1};
        Integer [] toCheck = {-1,-1};

        while (!gotInput) {
            // xy = player.getInput(board.getBoard(), player);

            xy[0] = this.onBoard[0];
            xy[1] = this.onBoard[1];
            toCheck[0] = xy[0];
            toCheck[1] = xy[1];
            if (board.getCellValue(toCheck) == 'O') {
                System.out.println("coordinates were already chosen, choose other coordinates");

            } else if (board.getCellValue(toCheck) == 'X') {
                System.out.println("coordinates were already chosen, choose other coordinates");

            } else {
                gotInput = true;
            }
            
        }

        

        if (board.checkForHit(toCheck)){
            System.out.println("Hit!");
            enemyBoard.changeCellValue(toCheck, 'X');
            board.changeCellValue(toCheck, 'X');
            // board.checkForHitHandler(xy);
            player.setPlayerDrowns(player.getPlayerDrowns() + 1);
        }
        else {
            System.out.println("Miss!");
            enemyBoard.changeCellValue(toCheck, 'O');
            board.changeCellValue(toCheck, 'O');
        }

        enemyBoard.printBoard();
        
    }

    private boolean checkForWin(int drowns) { // CHECKING FOR END GAME

        return (drowns == 17);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub

        Point p = e.getPoint().getLocation();
        int x = (int) p.getX();
        int y = (int) p.getY();
        Integer [] mouseXY = {x,y};

        this.onBoard[0] = null;
        this.onBoard[1] = null;

        // MyArray.print(mouseXY);
        if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 1) {

            for (int i = 0; i < this.cellValues.length; i++) {

                if (mouseXY[0] < this.tileLength*10 && mouseXY[1] < this.tileLength*10) {
                    if (this.cellValues[i][0] < mouseXY[0] && mouseXY[0] < this.cellValues[i][2]) {
                        this.onBoard[0] = i + 1;
                    }
        
                    if (this.cellValues[i][1] < mouseXY[1] && mouseXY[1] < this.cellValues[i][3]) {
                        this.onBoard[1] = i + 1;
                    }
                }
                
            }
            MyArray.print(onBoard);
            // System.out.println(this.hitsIndex);
            if (this.onBoard[0] != null && this.onBoard[1] != null && this.hitsIndex < this.hits.length) {
                this.hits[this.hitsIndex][0] = this.onBoard[0];
                this.hits[this.hitsIndex][1] = this.onBoard[1];
                this.hitsIndex++;
            }

            
        } else if (SwingUtilities.isRightMouseButton(e) && e.getClickCount() == 1) {

            for (int i = 0; i < this.cellValues.length; i++) {

                if (mouseXY[0] < this.tileLength*10 && mouseXY[1] < this.tileLength*10) {
                    if (this.cellValues[i][0] < mouseXY[0] && mouseXY[0] < this.cellValues[i][2]) {
                        this.onBoard[0] = i + 1;
                    }
        
                    if (this.cellValues[i][1] < mouseXY[1] && mouseXY[1] < this.cellValues[i][3]) {
                        this.onBoard[1] = i + 1;
                    }
                }
                
            }
            MyArray.print(onBoard);
            // System.out.println(this.hitsIndex);
            if (this.onBoard[0] != null && this.onBoard[1] != null && this.missesIndex < this.misses.length) {
                this.misses[this.missesIndex][0] = this.onBoard[0];
                this.misses[this.missesIndex][1] = this.onBoard[1];
                this.missesIndex++;
            }

        }

        
        repaint();

    }

    public void paint(Graphics g) {

        // MyArray.print(hits);

        g.drawImage(this.boardImage, 0,0,null);

        for (int i = 0; i < this.hits.length; i++) {
            if (this.hits[i][0] != null && this.hits[i][1] != null) {

                int x = this.hits[i][0] * this.tileLength - this.hitImage.getWidth(null);
                int y = this.hits[i][1] * this.tileLength - this.hitImage.getWidth(null);
                // System.out.println(x + " " + y);
                // g.drawImage(this.hitImage, x + 20,y + 20,null);
                g.drawImage(this.hitImage, x,y,null);
            }
        }

        for (int i = 0; i < this.misses.length; i++) {
            if (this.misses[i][0] != null && this.misses[i][1] != null) {

                int x = this.misses[i][0] * this.tileLength - this.missImage.getWidth(null);
                int y = this.misses[i][1] * this.tileLength - this.missImage.getWidth(null);
                // System.out.println(x + " " + y);
                // g.drawImage(this.hitImage, x + 20,y + 20,null);
                g.drawImage(this.missImage, x,y,null);
            }
        }

        for (int i = 0; i < this.boats.length; i ++) {
            if (this.boats[i] != null) {

                Integer [][] boatCoor = this.boats[i].getXyCoordinates();

                for (int j = 0; j < boatCoor.length; j++) {

                    int x = boatCoor[i][0] * this.tileLength - this.boatImage.getWidth(null);
                    int y = boatCoor[i][1] * this.tileLength - this.boatImage.getWidth(null);

                    g.drawImage(this.boatImage, x,y,null);

                }

                
            }
        }

    //     for (int i = 0; i < this.player1Board1.getBoard().length; i++) {

    //         for (int j = 0; j < this.player1Board1.getBoard()[i].length; j++) {

    //             if (this.player1Board1.getBoard()[i][j] == 'V') {
    //                 int x = (j + 1) * this.tileLength - this.boatImage.getWidth(null);
    //                 int y = (i + 1) * this.tileLength - this.boatImage.getWidth(null);
    //                 g.drawImage(this.boatImage, x,y,null);
    //             }
    //             if (this.player1Board1.getBoard()[i][j] == 'X') {
    //                 int x = (j + 1) * this.tileLength - this.hitImage.getWidth(null);
    //                 int y = (i + 1) * this.tileLength - this.hitImage.getWidth(null);
    //                 g.drawImage(this.hitImage, x,y,null);
    //             }
    //             if (this.player1Board1.getBoard()[i][j] == 'O') {
    //                 int x = (j + 1) * this.tileLength - this.missImage.getWidth(null);
    //                 int y = (i + 1) * this.tileLength - this.missImage.getWidth(null);
    //                 g.drawImage(this.missImage, x,y,null);
    //             }
            
    //         }

    //     }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }


}
