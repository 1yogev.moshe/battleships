import javax.swing.*;
import javax.swing.event.MouseInputListener;

import java.awt.*;
import java.awt.event.MouseEvent;


public class MyPanel extends JPanel implements MouseInputListener {

    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;

    private final Integer [][] cellValues = new Integer[10][4];
    private Integer [] onBoard = new Integer[2];

    private Integer [][] hits = new Integer[17][2];
    private int hitsIndex = 0;
    
    private Integer [][] misses = new Integer[100-17][2];
    private int missesIndex = 0;

    private Boat [] boats = new Boat[5];


    Image boardImage;
    Image hitImage;
    Image missImage;
    Image boatImage;

    private int tileLength;

    public MyPanel() {

        this.SCREEN_HEIGHT = 500;
        this.SCREEN_WIDTH = 1200;

        this.tileLength = 50;



        this.boardImage = new ImageIcon("res/board.png").getImage();
        this.hitImage = new ImageIcon("res/hit.png").getImage();
        this.missImage = new ImageIcon("res/miss.png").getImage();
        this.boatImage = new ImageIcon("res/boat.png").getImage();
        

        this.setPreferredSize(new Dimension(this.SCREEN_WIDTH,this.SCREEN_HEIGHT));

        int index = 0;
        for (int i = 0; i < this.tileLength*10; i+=this.tileLength) {
            this.cellValues[index][0] = i; // x1
            this.cellValues[index][1] = i; // y1
            this.cellValues[index][2] = i+this.tileLength; // x2
            this.cellValues[index][3] = i+this.tileLength; // y2
            index++;
        }
        

        this.addMouseListener(this);


        

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub

        Point p = e.getPoint().getLocation();
        int x = (int) p.getX();
        int y = (int) p.getY();
        Integer [] mouseXY = {x,y};

        this.onBoard[0] = null;
        this.onBoard[1] = null;

        // MyArray.print(mouseXY);
        if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 1) {

            for (int i = 0; i < this.cellValues.length; i++) {

                if (mouseXY[0] < this.tileLength*10 && mouseXY[1] < this.tileLength*10) {
                    if (this.cellValues[i][0] < mouseXY[0] && mouseXY[0] < this.cellValues[i][2]) {
                        this.onBoard[0] = i + 1;
                    }
        
                    if (this.cellValues[i][1] < mouseXY[1] && mouseXY[1] < this.cellValues[i][3]) {
                        this.onBoard[1] = i + 1;
                    }
                }
                
            }
            MyArray.print(onBoard);
            // System.out.println(this.hitsIndex);
            if (this.onBoard[0] != null && this.onBoard[1] != null && this.hitsIndex < this.hits.length) {
                this.hits[this.hitsIndex][0] = this.onBoard[0];
                this.hits[this.hitsIndex][1] = this.onBoard[1];
                this.hitsIndex++;
            }

            
        } else if (SwingUtilities.isRightMouseButton(e) && e.getClickCount() == 1) {

            for (int i = 0; i < this.cellValues.length; i++) {

                if (mouseXY[0] < this.tileLength*10 && mouseXY[1] < this.tileLength*10) {
                    if (this.cellValues[i][0] < mouseXY[0] && mouseXY[0] < this.cellValues[i][2]) {
                        this.onBoard[0] = i + 1;
                    }
        
                    if (this.cellValues[i][1] < mouseXY[1] && mouseXY[1] < this.cellValues[i][3]) {
                        this.onBoard[1] = i + 1;
                    }
                }
                
            }
            MyArray.print(onBoard);
            // System.out.println(this.hitsIndex);
            if (this.onBoard[0] != null && this.onBoard[1] != null && this.missesIndex < this.misses.length) {
                this.misses[this.missesIndex][0] = this.onBoard[0];
                this.misses[this.missesIndex][1] = this.onBoard[1];
                this.missesIndex++;
            }

        }

        
        repaint();

    }


    public void paint(Graphics g) {

        // MyArray.print(hits);

        g.drawImage(this.boardImage, 0,0,null);

        for (int i = 0; i < this.hits.length; i++) {
            if (this.hits[i][0] != null && this.hits[i][1] != null) {

                int x = this.hits[i][0] * this.tileLength - this.hitImage.getWidth(null);
                int y = this.hits[i][1] * this.tileLength - this.hitImage.getWidth(null);
                // System.out.println(x + " " + y);
                // g.drawImage(this.hitImage, x + 20,y + 20,null);
                g.drawImage(this.hitImage, x,y,null);
            }
        }

        for (int i = 0; i < this.misses.length; i++) {
            if (this.misses[i][0] != null && this.misses[i][1] != null) {

                int x = this.misses[i][0] * this.tileLength - this.missImage.getWidth(null);
                int y = this.misses[i][1] * this.tileLength - this.missImage.getWidth(null);
                // System.out.println(x + " " + y);
                // g.drawImage(this.hitImage, x + 20,y + 20,null);
                g.drawImage(this.missImage, x,y,null);
            }
        }

        for (int i = 0; i < this.boats.length; i ++) {
            if (this.boats[i] != null) {

                Integer [][] boatCoor = this.boats[i].getXyCoordinates();

                for (int j = 0; j < boatCoor.length; j++) {

                    int x = boatCoor[i][0] * this.tileLength - this.boatImage.getWidth(null);
                    int y = boatCoor[i][1] * this.tileLength - this.boatImage.getWidth(null);

                    g.drawImage(this.boatImage, x,y,null);

                }

                
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    public void placeBoats(Player player) { // PLACING ALL THE BOATS ON THE BOARD


        Integer [] boatsLength = {5,4,3,3,2};
        int boatsLengthIndex = 0;
        int boatIndex = 0;
        boolean passed = false;

        for (int i = 0; i < this.boats.length; i++,boatsLengthIndex++,boatIndex++) {

            passed = false;


            while (!passed) {

                System.out.println(player.getName() + " Place the " + boatsLength[boatsLengthIndex] + " length ship (starting point) : ");
    
                Integer [] xy = new Integer[2];

                boolean worksX = false;
                boolean worksY = false;
                boolean integ = false;

                int x = -1;
                int y = -1;

                boolean good = false;
    
                while (!good) {
    
                    while (!worksX) {

                        integ = false;
        
                        while (!integ) {
                            
                            try {
                                System.out.println("Enter x value (1-10) : ");
                                String str = sc.next();
                                x = Integer.parseInt(str);
                                integ = true;
                            } catch (Exception e) {
                                System.out.println("Please enter an integer.");
        
                            }
                            
                        }
                        
                        if (x < 1 || x > board[0].length) System.out.println("Index out of range.");
                        else worksX = true;
        
                    }
                    
                    
                    while (!worksY) {
        
                        integ = false;
        
                        while (!integ) {
                            
                            try {
                                System.out.println("Enter y value (1-10) : ");
                                String str = sc.next();
                                y = Integer.parseInt(str);
                                integ = true;
                            } catch (Exception e) {
                                System.out.println("Please enter an integer.");
        
                            }
                            
                        }
                        
                        if (y < 1 || y > board[0].length) System.out.println("Index out of range.");
                        else worksY = true;
        
                    }
        
                    xy[0] = x -1;
                    xy[1] = y -1;
    
                    if (this.getCellValue(xy) != '-'){
                        System.out.println("Index is already taken.");
                        worksX = false;
                        worksY = false;
                    }
                    else good = true;
    
                }
    
                good = false;
                Integer [] directionVec = new Integer[2]; 
    
                while (!good) {
    
                    System.out.println("What direction is the ship facing ? (right, left, up or down : ");
                    String direction = sc.next();
    
                    if (direction.toLowerCase().equals("right")) {
                        directionVec[0] = 1;
                        directionVec[1] = 0;
                        good = true;
                    } else if (direction.toLowerCase().equals("left")) {
                        directionVec[0] = -1;
                        directionVec[1] = 0;
                        good = true;
                    } else if (direction.toLowerCase().equals("down")) {
                        directionVec[0] = 0;
                        directionVec[1] = 1;
                        good = true;
                    } else if (direction.toLowerCase().equals("up")) {
                        directionVec[0] = 0;
                        directionVec[1] = -1;
                        good = true;
                    } else {
                        System.out.println("Not a valid direction.");
                    }
    
                }
    
                Boat tempBoat = new Boat(boatsLength[boatsLengthIndex], xy, directionVec);
                
                Integer [][] tempCoordinates = tempBoat.getXyCoordinates();
                
                int mistakes = 0;
                for (int j = 0; j < tempCoordinates.length; j++) {


                    if (tempCoordinates[j][0] > 9 || tempCoordinates[j][0] < 0 || tempCoordinates[j][1] > 9 || tempCoordinates[j][1] < 0) {
                        System.out.println("Ship cannot go there.");
                        mistakes++;
                        j = tempCoordinates.length + 1;
                    } else if (this.getCellValue(tempCoordinates[j]) != '-') {
                        System.out.println("Ship cannot go there.");
                        mistakes++;
                        j = tempCoordinates.length + 1;
                    }
                    

                }

                if (mistakes == 0) {
                    this.boats[boatIndex] = tempBoat;
                    passed = true;
                }

            }

            this.updateBoard(this.boats[boatIndex]);
            this.printBoard();


        }
        

    }

}
