public class Boat {

    private int len; // BOAT LENGTH
    private Integer [][] xyCoordinates; // ALL BOAT COORDINATES
    private Character [] xyCoordinatesValues; // ALL THE VALUES ON THE BOARD IN THESE COORDINATES 
    private boolean drowned; // IS THE BOAT DROWNED?
    private int index; // THE INDEX FOR LOPPING THROUGH xyCoordinatesValues

    public Boat(int len, Integer [] startsAt, Integer [] direction) { // CONSTRUCTOR
        this.len = len;
        this.xyCoordinates = new Integer[len][2];
        this.xyCoordinatesValues = new Character[len];
        this.index = 0;

        for (int i = 0; i < this.xyCoordinatesValues.length; i++) {
            this.xyCoordinatesValues[i] = 'V';
        }

        // MyArray.print(this.xyCoordinatesValues);

        this.drowned = false;
        this.initializeCoordinates(startsAt, direction);
    }

    public int getIndex() { // GETTING THE INDEX FOR LOPPING THROUGH xyCoordinatesValues
        return index;
    }

    public void setIndex(int index) { // SETTING THE INDEX FOR LOPPING THROUGH xyCoordinatesValues
        this.index = index;
    }

    public int getLen() { // GETTING BOAT LENGTH
        return this.len;
    }

    public Integer[][] getXyCoordinates() { // GETTING ALL BOAT COORDINATES
        return this.xyCoordinates;
    }

    public void initializeCoordinates(Integer [] startsAt, Integer [] direction) { // DIRECTION -> (1,0): right, (-1,0): left, (0,1): up, (0,-1): down

        int y = startsAt[1];
        int x = startsAt[0];

        int directionY = startsAt[1];
        int directionX = startsAt[0];

        int endX = -1;
        int endY = -1;

        if (directionX == -1) endX = x - this.len;
        if (directionX == 1) endX = x + this.len;

        if (directionY == -1) endY = y - this.len;
        if (directionY == 1) endY = y + this.len;

        int cellIndex = 0;
        boolean checking = true;

        while (checking) {


            try {
                this.xyCoordinates[cellIndex][0] = x;
                this.xyCoordinates[cellIndex][1] = y;
                cellIndex++;
            } catch (Exception e) {
                checking = false;
            }
            

            x += direction[0];
            y += direction[1];


        }


    }
    
    public Character[] getXyCoordinatesValues(){ // GETTING ALL THE VALUES ON THE BOARD IN THESE COORDINATES 
        return this.xyCoordinatesValues;
    }

    public void setXyCoordinatesValues(Character sign, int Index){ // SETTING ALL THE VALUES ON THE BOARD IN THESE COORDINATES 
        this.xyCoordinatesValues[Index] = sign;
    }

    public boolean isDrown() { // IS THE BOAT DROWN?

        int hits = 0;

        for (int i = 0; i < this.xyCoordinatesValues.length; i++) {
            if (this.xyCoordinatesValues[i] == 'X') hits++;
        }

        if (hits == len) this.drowned = true;

        return this.drowned;
    }


}
