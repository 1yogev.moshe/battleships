import java.rmi.dgc.Lease;
import java.util.Random;

public class BotiBot extends Player {

    Character [][] myGuesses = {{'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'}};
    private Character [][] board;
    private Integer [][] directions = {{1,0},{-1,0},{0,1},{0,-1}};
    private Random rand = new Random();
    // private Integer [][] moves = new Integer[100][2];
    // private int movesIndex = 0;

    // private Integer [] boatsLengths = {5,4,3,3,2};

    // private int steps = 0;


    public BotiBot(Character [][] board) {
        super("BotiBot");
        this.board = board;
        // MyArray.print(moves);
        this.run();

    }

    public Integer [] chooseRandomDirection(Integer [] cell) {

        int index = rand.nextInt(this.directions.length);
        boolean rendered = false;
        // MyArray.print(this.directions[index]);

        while (!rendered) {
            index = rand.nextInt(this.directions.length);

            if (cell[0] == 0 || cell[0] == 9 || cell[1] == 0 || cell[1] == 9) {
                
                // if ((cell[0] == 0 && this.directions[index][0] == -1) || 
                //     (cell[0] == 9 && this.directions[index][0] == 1) ||
                //     (cell[1] == 0 && this.directions[index][1] == -1) ||
                //     (cell[1] == 9 && this.directions[index][1] == 1)) {

                //     index = rand.nextInt(this.directions.length);
                //     // MyArray.print(this.directions[index]);
                // } else {
                //     return this.directions[index];
                // }

                if (cell[0] == 0 && this.directions[index][0] != -1 && ((cell[1] == 0 && this.directions[index][1] != -1) || (cell[1] == 9 && this.directions[index][0] != 1))) {
                    return this.directions[index];
                }

                if (cell[0] == 9 && this.directions[index][0] != 1 && ((cell[1] == 0 && this.directions[index][1] != -1) || (cell[1] == 9 && this.directions[index][0] != 1))) {
                    return this.directions[index];
                }

                if (cell[1] == 0 && this.directions[index][1] != -1 && ((cell[0] == 0 && this.directions[index][0] != -1) || (cell[0] == 9 && this.directions[index][0] != 1))) {
                    return this.directions[index];
                }

                if (cell[1] == 9 && this.directions[index][0] != 1 && ((cell[0] == 0 && this.directions[index][0] != -1) || (cell[0] == 9 && this.directions[index][0] != 1))) {
                    return this.directions[index];
                }


            } else {
                return this.directions[index];
            
        }
        
        }

        return null;
    }

    public Integer [] chooseRandomCell() {
        
        
        int x = rand.nextInt(this.board.length);
        int y = rand.nextInt(this.board.length);

        Integer [] cell = {x,y};
        boolean goody = false;

        if (this.board[cell[1]][cell[0]].equals('X') && this.myGuesses[cell[1]][cell[0]].equals('-')) {
            this.myGuesses[cell[1]][cell[0]] = 'X';
            // this.steps++;
            MyArray.print(this.myGuesses);
            return cell;
        } else if (this.board[cell[1]][cell[0]].equals('-')) {
            this.myGuesses[cell[1]][cell[0]] = 'O';
        }

        while (!goody) {

            x = rand.nextInt(this.board.length);
            y = rand.nextInt(this.board.length);

            cell[0] = x;
            cell[1] = y;

            if (this.board[cell[1]][cell[0]].equals('X') && this.myGuesses[cell[1]][cell[0]].equals('-')) {
                this.myGuesses[cell[1]][cell[0]] = 'X';
                // this.steps++;
                MyArray.print(this.myGuesses);
                return cell;
            } else if (this.board[cell[1]][cell[0]].equals('-')) {
                this.myGuesses[cell[1]][cell[0]] = 'O';
            }

        }

        

        return null;

    }

    public void findBoat() {
        System.out.println("1");
        Integer [] StartingRandomCell = this.chooseRandomCell();
        Integer [] currentCell = StartingRandomCell;

        // MyArray.print(this.myGuesses);
        // System.out.println();

        // ------- ALL GOOD ------------------------------
        System.out.println("2");
        Integer [] direction = {0,0};//this.chooseRandomDirection(currentCell);
        boolean goodDirection = false;
        // MyArray.print(direction);
        while (!goodDirection) {

            direction = this.chooseRandomDirection(currentCell);
            // MyArray.print(direction);
            System.out.println("3");
            currentCell[0] += direction[0];
            currentCell[1] += direction[1];


            if (this.board[currentCell[1]][currentCell[0]].equals('-') || this.myGuesses[currentCell[1]][currentCell[0]].equals('O')) {
                this.myGuesses[currentCell[1]][currentCell[0]] = 'O';
                currentCell[0] -= direction[0];
                currentCell[1] -= direction[1];
                System.out.println("4");

            } else if (this.board[currentCell[1]][currentCell[0]].equals('X')) {
                this.myGuesses[currentCell[1]][currentCell[0]] = 'X';
                // this.steps++;
                goodDirection = true;
                System.out.println("5");
            }

        }

        // MyArray.print(direction);
        // MyArray.print(this.myGuesses);
        System.out.println("6");

        boolean going = true;

        while (going) {

            currentCell[0] += direction[0];
            currentCell[1] += direction[1];

            if (0 > currentCell[0] || currentCell[0] >= this.board.length || 0 > currentCell[1] || currentCell[1] >= this.board.length) {
                currentCell = StartingRandomCell;
                going = false;

            } else if (this.board[currentCell[1]][currentCell[0]].equals('-')) {
                this.myGuesses[currentCell[1]][currentCell[0]] = 'O';
                currentCell = StartingRandomCell;
                going = false;
            } else {
                // this.steps++;
                this.myGuesses[currentCell[1]][currentCell[0]] = 'X';
            }


        }

        // MyArray.print(this.myGuesses);

        direction[0] *= -1;
        direction[1] *= -1;

        going = true;

        System.out.println("4");

        while (going) {

            currentCell[0] += direction[0];
            currentCell[1] += direction[1];

            if (0 > currentCell[0] || currentCell[0] >= this.board.length || 0 > currentCell[1] || currentCell[1] >= this.board.length) {
                // currentCell = StartingRandomCell;
                going = false;
            } else if (this.board[currentCell[1]][currentCell[0]].equals('-')) {
                this.myGuesses[currentCell[1]][currentCell[0]] = 'O';
                // currentCell = StartingRandomCell;
                going = false;
            } else {
                // this.steps++;
                this.myGuesses[currentCell[1]][currentCell[0]] = 'X';
            }


        }


        // MyArray.print(this.myGuesses);
        // System.out.println(this.steps);
        System.out.println("5");
    }

    public void run() {

        // for (int i = 0; i < 5; i++) {
        //     this.findBoat();
        // }

        this.findBoat();
        MyArray.print(this.myGuesses);
        this.findBoat();
        MyArray.print(this.myGuesses);
        this.findBoat();
        MyArray.print(this.myGuesses);
        this.findBoat();
        MyArray.print(this.myGuesses);
        this.findBoat();
        MyArray.print(this.myGuesses);

    }

    public static void main(String[] args) {

        Character [][] board = {{'X', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'X', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'X', '-', '-', '-', 'X', 'X', '-', '-', 'X', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', 'X', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', 'X', '-'},
                                {'-', '-', '-', 'X', 'X', 'X', 'X', 'X', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'}, 
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
                                {'-', '-', 'X', 'X', 'X', 'X', '-', '-', '-', '-'},
                                {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'}};

        

        BotiBot bb = new BotiBot(board);

        // Integer [] cell = {0,0};

        // MyArray.print(bb.chooseRandomDirection(cell));

    }

    
}
