public class MyArray {
    
    public static <T> void print(T[] arr) {
        System.out.print("{");
        for (int i = 0; i < arr.length; i++) {
            if (i != arr.length - 1) System.out.print(arr[i] + ", ");
            else System.out.print(arr[i]);
        }
        System.out.println("}");
    }

    public static <T> void print(T[][] arr) {
        System.out.print("{");
        for (int i = 0; i < arr.length; i++) {
            System.out.print("{");
            for (int j = 0; j < arr[0].length; j++) {
                if (j != arr[0].length - 1) System.out.print(arr[i][j] + ", ");
                else System.out.print(arr[i][j]);
            }
            if (i != arr.length - 1) System.out.println("}, ");
            else System.out.print("}");
        }
        System.out.println("}");
    }

    public static Integer maxValue(Integer[] arr) {
        
        Integer max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) max = arr[i];
        }

        return max;
//        System.out.println(max);
    }

    public static Double maxValue(Double[] arr) {
        
        Double max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) max = arr[i];
        }

        return max;
//        System.out.println(max);
    }

    public static int maxIndex(Integer [] arr) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > arr[max]) max = i;
        }

        return max;
//        System.out.println(max);


    }

    public static int maxIndex(Double [] arr) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > arr[max]) max = i;
        }

        return max;
//        System.out.println(max);


    }

    public static void allZero(Integer [] arr) {
        for (int i =0; i < arr.length; i++) {
            arr[i] = 0;
        }
    }

    public static void allZero(Double [] arr) {

    
        for (int i =0; i < arr.length; i++) {
            arr[i] = 0.0;
        }
    }

    public static Character [] toChars(String str) {

        Character [] chars = new Character[str.length()];

        for (int i = 0; i < str.length(); i++) {
            chars[i] = str.charAt(i);
        }

        return chars;

    }


}
