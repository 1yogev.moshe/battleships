import java.util.Scanner;

public class Board {

    // private Character [][] board = {{'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', 'O', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
    //                                 {'-', '-', '-', '-', '-', '-', '-', '-', '-', '-'}};
    private Character [][] board; // A BOARD IN A GIVEN DIMENSIONS
    private Boat [] boats; // ALL THE BOATS WHICH ARE ON THIS BOARD
    private static Scanner sc = new Scanner(System.in); // LOCAL SCANNER
    
    /**
     * 
     * @param length  the length the board will have
     */
    public Board(int length) { // CONSTRUCTOR
        this.board = new Character[length][length];
        this.initializeBoard(this.board);
        this.boats = new Boat[5];
    }


    /**
     * 
     * @param board
     */
    private void initializeBoard(Character [][] board) { // SETTING ALL CELLS IN THE BOARD TO '-'
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                board[i][j] = '-';
            }
        }
    }



    public void printBoard() { // PRINTING THE BOARD IN A SPECIFIC LAYOUT
        int num = 1;
        System.out.println("   1 2 3 4 5 6 7 8 9 10 ");
        for (int i = 0; i < this.board.length; i++) {
            System.out.print(num + " ");
            for (int j = 0; j < this.board[0].length; j++) {
                if (j == this.board[0].length - 1) System.out.println(this.board[i][j]);
                else if (j == 0 && num != 10) System.out.print(" " + this.board[i][j] + " ");
                else System.out.print(this.board[i][j] + " ");
            }
            num++;
        }
    }


    /**
     * 
     * @param xy a coordinate
     * @return a cell value in a specific coordinate
     */
    public char getCellValue(Integer[] xy){ // GETTING THE CELL VALUE AT A SPECIFIC COORDINATE
        return this.board[xy[1]][xy[0]];
    }
    
    /**
     * 
     * @param xy a coordinate 
     * @return whether its a hit or a miss
     */
    public boolean checkForHit(Integer[] xy){ // CHECKING FOR HIT IN A SPECIFIC COORDINATE
        return (this.getCellValue(xy) == 'V');
        
    }

    /**
     * 
     * @param xy a coordinate
     */
    public void checkForHitHandler(Integer[] xy) { // HANDLING WHEN HIT OCCURS
        for (int i = 0; i < this.boats.length; i++) {
            Integer [][] tempCoors = this.boats[i].getXyCoordinates();
            for (int j = 0; j < tempCoors.length; j++) {
                if (xy == tempCoors[j]) {
                    this.boats[i].setXyCoordinatesValues('X', this.boats[i].getIndex());
                    this.boats[i].setIndex(this.boats[i].getIndex() + 1);
                }
            }
        }
    }


    /**
     * 
     * @param xy a coordinate
     * @param sign the given value the cell will be changed to 
     */
    public void changeCellValue(Integer[] xy, char sign){ // CHANGING  A CELL VALUE WITH A GIVEN VALUE
        this.board[xy[1]][xy[0]] = sign;
    }


    /**
     * 
     * @param drowns the number of hits
     * @return whether its end game
     */
    public boolean checkForWin(int drowns) { // CHECKING FOR END GAME

        // int drowned = 0;

        // for (int i = 0; i < this.boats.length; i++) {
        //     if (this.boats[i].isDrown()) drowned++;
        // }

        // return (drowned == 5);
        return (drowns == 17);

    }

    /**
     * 
     * @param player the player who places the boats 
     */
    public void placeBoats(Player player) { // PLACING ALL THE BOATS ON THE BOARD


        Integer [] boatsLength = {5,4,3,3,2};
        int boatsLengthIndex = 0;
        int boatIndex = 0;

        for (int i = 0; i < this.boats.length; i++,boatsLengthIndex++,boatIndex++) {

            this.getXYInput(player, boatsLength, boatsLengthIndex, boatIndex);

        }
        

    }

    /**
     * 
     * @param player the player who places the boat
     * @param boatsLength an array which holds the length of the boats 
     * @param boatsLengthIndex the index to use in the @param boatsLength array
     * @param boatIndex the index to use in this.boats
     */
    public void getXYInput(Player player, Integer [] boatsLength, int boatsLengthIndex, int boatIndex) { // placing one boat

        boolean passed = false;

        while (!passed) {

            System.out.println(player.getName() + " Place the " + boatsLength[boatsLengthIndex] + " length ship (starting point) : ");

            Integer [] xy = new Integer[2];

            boolean worksX = false;
            boolean worksY = false;
            boolean integ = false;

            int x = -1;
            int y = -1;

            boolean good = false;

            while (!good) {

                while (!worksX) {

                    x = this.getInteger('x', player);
                    
                    if (x < 1 || x > board[0].length) System.out.println("Index out of range.");
                    else worksX = true;
    
                }
                
                
                while (!worksY) {
    
                    y = this.getInteger('y', player);
                    
                    if (y < 1 || y > board[0].length) System.out.println("Index out of range.");
                    else worksY = true;
    
                }
    
                xy[0] = x -1;
                xy[1] = y -1;

                if (this.getCellValue(xy) != '-'){
                    System.out.println("Index is already taken.");
                    worksX = false;
                    worksY = false;
                }
                else good = true;

            }

            good = false;
            Integer [] directionVec = new Integer[2]; 

            while (!good) {

                System.out.println("What direction is the ship facing ? (right, left, up or down : ");
                String direction = sc.next();

                if (this.isGood(direction)) {
                    directionVec = this.getDirection(direction);
                    good = true;
                } else {
                    System.out.println("Not a valid direction.");
                }

            }

            Boat tempBoat = new Boat(boatsLength[boatsLengthIndex], xy, directionVec);
            
            Integer [][] tempCoordinates = tempBoat.getXyCoordinates();
            
            
            if (this.checkDirection(tempCoordinates) == 0) {
                this.boats[boatIndex] = tempBoat;
                passed = true;
            }

        }

        this.updateBoard(this.boats[boatIndex]);
        this.printBoard();


    }

    /**
     * getting an integer input
     * @param sign the sign of the value your getting
     * @return the value we got
     */
    public int getInteger(char sign, Player player) {
        boolean integ = false;
        int value = 0;

        while (!integ) {
                    
            try {
                System.out.println(player.getName() + " Enter " + sign + " value (1-10) : ");
                String str = sc.next();
                value = Integer.parseInt(str);
                integ = true;
            } catch (Exception e) {
                System.out.println(player.getName() + " Please enter an integer.");

            }
            
        }

        return value;
    }


    /**
     * 
     * @param tempCoordinates the boat coordinates
     * @return the number of mistakes
     */
    public int checkDirection(Integer [][] tempCoordinates) { // checks the limits of the board
        int mistakes = 0;
        for (int j = 0; j < tempCoordinates.length; j++) {


            if (tempCoordinates[j][0] > 9 || tempCoordinates[j][0] < 0 || tempCoordinates[j][1] > 9 || tempCoordinates[j][1] < 0) {
                System.out.println("Ship cannot go there.");
                mistakes++;
                j = tempCoordinates.length + 1;
            } else if (this.getCellValue(tempCoordinates[j]) != '-') {
                System.out.println("Ship cannot go there.");
                mistakes++;
                j = tempCoordinates.length + 1;
            }
            

        }

        return mistakes;
    }

    /**
     * 
     * @param direction the input from the player
     * @return the direction vector
     */
    public Integer [] getDirection(String direction) { // getting the direction form an input

        Integer [] directionVec = new Integer[2];

        if (direction.toLowerCase().equals("right")) {
            directionVec[0] = 1;
            directionVec[1] = 0;

        } else if (direction.toLowerCase().equals("left")) {
            directionVec[0] = -1;
            directionVec[1] = 0;

        } else if (direction.toLowerCase().equals("down")) {
            directionVec[0] = 0;
            directionVec[1] = 1;

        } else if (direction.toLowerCase().equals("up")) {
            directionVec[0] = 0;
            directionVec[1] = -1;
        }

        return directionVec;
    }  

    /**
     * 
     * @param direction the player's input
     * @return whether its valid input
     */
    public boolean isGood(String direction) { // checks the direction input 
        if (direction.equals("right") || 
        direction.equals("left") || 
        direction.equals("up") || 
        direction.equals("down")) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 
     * @param b a boat 
     */
    public void updateBoard(Boat b) { // UPDATING THE BOARD - GIVEN COORDINATES => A SPECIFIC VALUE

        Integer [][] Coordinates = b.getXyCoordinates();
    
        for (int j = 0; j < Coordinates.length; j++) {

            this.changeCellValue(Coordinates[j], 'V');

        }
    }

    /**
     * 
     * @return the character board
     */
    public Character[][] getBoard() { // RETURNING THE BOARD - THE CHARACTERS ARRAY
        return this.board;
    }

    /**
     * 
     * @param board the board that the current board well be changed to
     */
    public void setBoard(Character[][] board) { // SETTING THE BOARD - THE CHARACTERS ARRAY
        this.board = board;
    }

    // public static void main(String[] args) {
    //     Board b = new Board(10);
    //     b.printBoard();
    // }


}


